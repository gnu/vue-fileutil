import { invoke } from '@tauri-apps/api/tauri'

export const sendCommand = async (command: string, params?: any) => {
    const fileList = await invoke(command, params);
    console.log(`callRustFun[${command}] Resp: ${fileList}`);
}