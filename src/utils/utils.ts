import { ScanFileResult } from "../model/model";

/**
     * 将秒数换成时分秒格式
     */
export const formatSeconds = (secondTime: number): string => {
    let minuteTime = 0;// 分
    let hourTime = 0;// 小时
    if (secondTime > 60) {//如果秒数大于60，将秒数转换成整数
        //获取分钟，除以60取整数，得到整数分钟
        minuteTime = parseInt(String(secondTime / 60));
        //获取秒数，秒数取佘，得到整数秒数
        secondTime = parseInt(String(secondTime % 60));
        //如果分钟大于60，将分钟转换成小时
        if (minuteTime > 60) {
            //获取小时，获取分钟除以60，得到整数小时
            hourTime = parseInt(String(minuteTime / 60));
            //获取小时后取佘的分，获取分钟除以60取佘的分
            minuteTime = parseInt(String(minuteTime % 60));
        }
    }
    let result = "" + parseInt(String(secondTime)) + " 秒";

    if (minuteTime > 0) {
        result = "" + parseInt(String(minuteTime)) + " 分" + result;
    }
    if (hourTime > 0) {
        result = "" + parseInt(String(hourTime)) + " 小时" + result;
    }
    return result;
}


export const formatFileSize = (fileSize: number): string => {
    if (fileSize < 1024) {
        return fileSize + ' B';
    } else if (fileSize < (1024 * 1024)) {
        return (fileSize / 1024).toFixed(2) + ' KB';
    } else if (fileSize < (1024 * 1024 * 1024)) {
        return (fileSize / (1024 * 1024)).toFixed(2) + ' MB';
    } else {
        return (fileSize / (1024 * 1024 * 1024)).toFixed(2) + ' GB';
    }
}

export const sortByFileSize = (list: ScanFileResult[]): ScanFileResult[] => {
    return list.sort((a, b) => {
        return b.files[0].size - a.files[0].size;
    });
}