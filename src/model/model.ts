export interface ScanDir {
    dir: string;
}

export interface FileModel {
    md5: string;
    name: string;
    num: number;
    path: string;
    size: number;
    sizeStr: string;
    pathArray: string[];
}

export interface ScanFileResult {
    md5: string,
    files: ScanFile[]
}

export interface ScanFile {
    file_name: string,
    size: number,
    file_path: string,
    extension: string,
}